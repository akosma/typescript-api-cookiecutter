= TypeScript API Project Cookiecutter

Use this project to create a TypeScript API project.

== Usage

. Install https://cookiecutter.readthedocs.io/[Cookiecutter]
. Run `cookiecutter https://gitlab.com/akosma/typescript-api-cookiecutter`

Follow the instructions in the README of the new project.
