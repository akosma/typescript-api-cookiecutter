const gulp = require('gulp')
const uglify = require('gulp-uglify')
const chmod = require('gulp-chmod')
const insert = require('gulp-insert')
const ts = require('gulp-typescript')

function buildTypeScript () {
  const tsProject = ts.createProject('tsconfig.json')
  return gulp.src(['src/*.ts'])
    .pipe(tsProject())
    .pipe(gulp.dest('dist'))
}

function uglifyJavaScript () {
  return gulp.src('dist/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
}

function makeExecutable () {
  return gulp.src('dist/index.js')
    .pipe(insert.prepend('#!/usr/bin/env node\n'))
    .pipe(chmod(0o755))
    .pipe(gulp.dest('dist'))
}

exports.build = gulp.task("build", gulp.series(buildTypeScript))
exports.release = gulp.task("release", gulp.series(buildTypeScript, uglifyJavaScript, makeExecutable))
