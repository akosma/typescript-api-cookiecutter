import express from 'express'
import path from 'path'
import { Customer } from './customer'

process.on('SIGINT', () => {
  process.exit(0)
})

const app = express()
app.use('/', express.static(path.join(process.cwd(), 'public')))
const port = 3000

app.get('/greet/:name', (req, res) => {
  const cust = new Customer(req.params.name)
  res.send(cust.greet())
})

app.listen(port, () => {
  console.log(`Example app listening port ${port}`)
})
