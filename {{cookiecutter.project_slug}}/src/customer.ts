import { DateTime } from 'luxon'

/**
 * This class represents a customer
 */
export class Customer {
  readonly name: string

  /**
   * Represents a customer
   * @param name name of the customer
   */
  constructor(name: string) {
    this.name = name
  }

  /**
   * Greets the user
   */
  greet(): string {
    return `Hello ${this.name}: ${DateTime.now().toHTTP()}`
  }
}
